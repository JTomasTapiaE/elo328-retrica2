# ELO328-Retrica2
 
 Proyecto enfocado en la deteccion de caracteres de la patente de un vehiculo en tiempo real.

 Para la ejecucion del programa necesitames tener python y las siguientes librerias:
 -OpenCv
 -Tesseract

Al momento de enfocar la camara en la patente, debemos ver si enfoca los caracteres claramente.

Para finalizar presionamos la letra "s" y mostrara los caracteres por consola, filtrando aquellos que no pertenezcan a la patente.
