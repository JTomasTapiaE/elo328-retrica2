import cv2
import pytesseract
import skimage
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'


frameWidth = 640    #Frame Width
franeHeight = 480   # Frame Height

plateCascade = cv2.CascadeClassifier("dataset.xml")
minArea = 500

cap =cv2.VideoCapture(0)
cap.set(3,frameWidth)
cap.set(4,franeHeight)
cap.set(10,150)

while True:
    success , img  = cap.read()

    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #thresh=cv2.threshold(imgGray,170,255,cv2.THRESH_BINARY_INV)[1]


    numberPlates = plateCascade.detectMultiScale(imgGray, 1.1, 4)

    for (x, y, w, h) in numberPlates:
        area = w*h
        if area > minArea:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            cv2.putText(img,"Numero de Placa",(x,y-5),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)
            imgRoi = img[y:y+h,x:x+w]
            #cv2.imshow("ROI",imgRoi)
    cv2.imshow("Resultado",img)
    if cv2.waitKey(1) & 0xFF ==ord('s'):
        cv2.rectangle(img,(0,200),(640,300),(0,255,0),cv2.FILLED)
        cv2.putText(img,"Scan Saved",(15,265),cv2.FONT_HERSHEY_COMPLEX,2,(0,0,255),2)
        #cv2.imshow("Result",img)
        cv2.waitKey(500)
        gray = cv2.cvtColor(imgRoi, cv2.COLOR_BGR2GRAY)
        thresh=cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY_INV,7,13)
        borderless=skimage.segmentation.clear_border(thresh)

        final=cv2.bitwise_not(borderless)
        options="-c tessedit_char_whitelist=BCDFGHJKLPRSTVWXYZ0123456789 --psm 7"

        text = pytesseract.image_to_string(final,config=options)
        print(text)
        cv2.imshow('Image',final)
        cv2.waitKey(0)
        break